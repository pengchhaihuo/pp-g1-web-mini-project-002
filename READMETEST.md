# MyFace

## Authentication (AuthenticationRestController)
Create account\
Login then create a profile
## User Profile (ProfileRestController)
Create a profile\
Update profile\
View profiles by ID\
Close your profile and account\
Note: no owner checking yet
## CV Builder (CV Rest Controller)
Get CV by ID\
Generate CV from profile information\
Create and edit new CV\
Update existing CV by ID\
Rename CV \
Close (Delete) CV
## Cover Letter Builder (Cover Letter Rest Controller)
Create new cover letter\
Get list of cover letters by profile ID\
Get cover letter by cover letter ID\
Update cover letter\
Delete Cover Letter
