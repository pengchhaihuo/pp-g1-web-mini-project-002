import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import { Card, Dropdown } from "react-bootstrap";
import moment from "moment";
import PopupList from "../components/PopupList";
import Pagination from "./Pagination";

export default function ListCard({ users, onDelete, onUpdate }) {
    const [currentPage, setCurrentPage] = useState(1);
    const [rowsPerPage, setRowsPerPage] = useState(4);

    const indexOfLastPage = currentPage * rowsPerPage;
    const indexOfFirstPage = indexOfLastPage - rowsPerPage;
    const currentRows = users.slice(indexOfFirstPage, indexOfLastPage);

    const paginate = (number) => {
        console.log(number + 1);
        setCurrentPage(number + 1);
    };

    const setRowSize = (size) => {
        setRowsPerPage(size);
    };
    return (
        <div>
            <div className='row'>
                {users.map((user, index) => (
                    <div key={index} className='col-md-3 mb-4'>
                        <Card className='h-100' style={{ fontWeight: "bold" }}>
                            <Card.Header className='text-center'>
                                <Dropdown>
                                    <Dropdown.Toggle variant='success' id='dropdown-basic'>
                                        Action
                </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        <Dropdown.Item>
                                            <PopupList text={"View"} data={user}></PopupList>
                                        </Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={() => onUpdate(user)}>
                                            Update
                  </Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={() => onDelete(user.id)}>
                                            Delete
                  </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Card.Header>
                            <Card.Body>
                                <h3>{user.name}</h3>
                                <h4>Jobs : </h4>
                                <h6>{user.job}</h6>
                            </Card.Body>
                            <Card.Footer className='text-muted text-center'>
                                {moment(user.createdAt, "YYYY-MM-DD h:mm:ss a").fromNow()}
                            </Card.Footer>
                        </Card>
                    </div>
                ))}
            </div>
            <div>
            <Pagination
                rowsPerPage={4}
                currentPage={currentPage}
                totalEntries={users.length}
                paginate={paginate}
            />
            </div>
        </div>
    );
}
