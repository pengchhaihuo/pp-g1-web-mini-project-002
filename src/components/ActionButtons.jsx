import React from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import Popup from "../components/Popup";
import { BsPencilSquare, BsFillTrashFill } from "react-icons/bs";

export default function ActionButtons({ user, onDelete, onUpdate }) {
  return (
    <ButtonGroup>
      <Popup text={"View"} variant='primary' data={user}></Popup>
      <Button size='sm' variant='success' onClick={() => onUpdate(user)}>
        <BsPencilSquare /> Update
      </Button>{" "}
      <Button size='sm' variant='danger' onClick={() => onDelete(user.id)}>
        <BsFillTrashFill /> Delete
      </Button>
    </ButtonGroup>
  );
}
