import "bootstrap/dist/css/bootstrap.min.css";
import { Table } from "react-bootstrap";
import ActionButtons from "./ActionButtons";
import React, { useState } from "react";
import moment from "moment";
import Pagination from "./Pagination";

export default function MyTable({ users, onDelete, onUpdate }) {
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(4);

  const indexOfLastPage = currentPage * rowsPerPage;
  const indexOfFirstPage = indexOfLastPage - rowsPerPage;
  const currentRows = users.slice(indexOfFirstPage, indexOfLastPage);

  const paginate = (number) => {
    console.log(number + 1);
    setCurrentPage(number + 1);
  };

  return (
    <>
      <Pagination
        rowsPerPage={4}
        currentPage={currentPage}
        totalEntries={users.length}
        paginate={paginate}
      />
      <Table bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Job</th>
            <th>Create At</th>
            <th>Update At</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {currentRows.map((user, index) => (
            <tr key={index}>
              <td>{user.id}</td>
              <td>{user.name}</td>
              <td>{user.gender}</td>
              <td>{user.email}</td>
              <td>{user.job}</td>
              <td>
                {moment(user.createdAt, "YYYY-MM-DD h:mm:ss a").fromNow()}
              </td>
              <td>{user.updatedAt}</td>
              <td>
                <ActionButtons
                  user={user}
                  onDelete={onDelete}
                  onUpdate={onUpdate}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <div class='d-flex justify-content-center align-items-center'>
        {users.length <= 0 && (
          <div>
            <img src={"https://static.thenounproject.com/png/82078-200.png"} />
            <h3 class='text-center'>list is empty</h3>
          </div>
        )}
        {users.length > 0 && (
          <Pagination
            rowsPerPage={4}
            currentPage={currentPage}
            totalEntries={users.length}
            paginate={paginate}
          />
        )}
      </div>
    </>
  );
}
