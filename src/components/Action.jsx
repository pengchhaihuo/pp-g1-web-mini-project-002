export const changeToTable = () => {
    document.getElementById("table").style.display = "block";
    document.getElementById("card").style.display = "none";
};

export const changeToCard = () => {
    document.getElementById("table").style.display = "none";
    document.getElementById("card").style.display = "block";
};