import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Row, Col, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import { validateUsername, validateEmail } from "./Validation";

const emptyUser = {
  name: "",
  gender: "",
  email: "",
  job: [],
  createdAt: "",
  updatedAt: "",
};

export default function MyForm({ onAdd, formUser, isUpdating }) {
  const [user, setUser] = useState({
    name: "",
    gender: "",
    email: "",
    job: [],
    createdAt: "",
    updatedAt: "",
  });

  let [isValidUsername, usernameWarning] = [true, ""]; //validateUsername(user.name);
  let [isValidEmail, emailWarning] = [true, ""]; //validateEmail(user.email);

  const handleInputChange = (event) => {
    if (event.target.name === "job") {
      setUser((user) => ({
        ...user,
        [event.target.name]: [...user.job, event.target.value],
      }));
    } else {
      setUser((user) => ({
        ...user,
        [event.target.name]: event.target.value,
      }));
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    clearForm();
  };

  const clearForm = () => {
    setUser((user) => ({
      ...emptyUser,
    }));
  };

  useEffect(() => {
    setUser((user) => ({
      ...formUser,
    }));
    console.log(user);
  }, [isUpdating, setUser]);

  return (
    <>
      <h1 className='py-3 variant-secondary'>Personal Info</h1>
      <div>
        <Row className='p-0'>
          <Col md={8}>
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId='formBasicUserName' style={{ margin: 0 }}>
                <Form.Label style={{ fontWeight: "bold" }}>Username</Form.Label>
                <Form.Control
                  size='sm'
                  type='text'
                  placeholder='username'
                  value={user.name}
                  name='name'
                  onChange={handleInputChange}
                />
                <Form.Text
                  className=' text-danger'
                  style={{
                    color: "red",
                    display: isValidUsername ? "none" : "block",
                  }}>
                  {usernameWarning}
                </Form.Text>
              </Form.Group>
              <Form.Group controlId='formBasicEmail' style={{ margin: 0 }}>
                <Form.Label style={{ fontWeight: "bold" }}>
                  Email address
                </Form.Label>
                <Form.Control
                  size='sm'
                  type='email'
                  placeholder='name@example.com'
                  value={user.email}
                  onChange={handleInputChange}
                  name='email'
                />
                <Form.Text
                  className='text-danger'
                  style={{
                    color: "red",
                    display: isValidEmail ? "none" : "block",
                  }}>
                  {emailWarning}
                </Form.Text>
              </Form.Group>
              <div style={{ textAlign: "right" }}>
                <Button
                  variant='primary'
                  size='sm'
                  onClick={(e) => {
                    [isValidUsername, usernameWarning] = validateUsername(
                      user.name
                    );
                    [isValidEmail, emailWarning] = validateEmail(user.email);
                    if (isValidEmail && isValidUsername) {
                      onAdd(e, user);
                      clearForm();
                    }
                  }}>
                  Submit
                </Button>
                {"  "}
                <Button
                  variant='secondary'
                  size='sm'
                  type='submit'
                  onClick={() => clearForm()}>
                  Cancel
                </Button>
              </div>
            </Form>
          </Col>
          <Col>
            <Form>
              <Form.Group>
                <h5 style={{ fontWeight: "bold" }}> Gender</h5>
                <Form.Check
                  inline
                  label='Male'
                  name='gender'
                  type='radio'
                  id='m'
                  value='Male'
                  defaultChecked={true}
                  checked={user.gender === "Male"}
                  onChange={handleInputChange}
                />
                <Form.Check
                  inline
                  label='Female'
                  name='gender'
                  type='radio'
                  id='f'
                  value='Female'
                  checked={user.gender === "Female"}
                  onChange={handleInputChange}
                />
              </Form.Group>
              <Form.Group>
                <h5 style={{ fontWeight: "bold" }}>Job</h5>
                <Form.Check
                  inline
                  label='Student'
                  name='job'
                  type='checkbox'
                  id='student'
                  value='Student'
                  onChange={handleInputChange}
                />
                <Form.Check
                  inline
                  label='Teacher'
                  name='job'
                  type='checkbox'
                  id='teacher'
                  value='Teacher'
                  checked={user.job === "Developer"}
                  onChange={handleInputChange}
                />
                <Form.Check
                  inline
                  label='Developer'
                  name='job'
                  type='checkbox'
                  id='developer'
                  value='Developer'
                  checked={user.job === "Developer"}
                  onChange={handleInputChange}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </div>
    </>
  );
}
