import React from "react";
import * as onAction from "./Action";
import { Button, ButtonGroup } from "react-bootstrap";

export default function Toggle() {
  return (
    <div>
      <div>
        <h2 className='py-2'>Display Data As:</h2>
        <ButtonGroup className='pb-3' size='sm' aria-label='Basic example'>
          <Button
            variant='dark'
            disabled={false}
            onClick={onAction.changeToTable}>
            Table
          </Button>
          <Button variant='secondary' onClick={onAction.changeToCard}>
            Card
          </Button>
        </ButtonGroup>
      </div>
    </div>
  );
}
