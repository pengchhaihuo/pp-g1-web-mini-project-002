import React from "react";
import { useState, useEffect } from "react";

export const validateUsername = (username) => {
  let message = "";
  let valid = false;
  if (username === "") {
    valid = false;
    message = "Username cannot be empty.";
  } else if (!isNaN(username)) {
    valid = false;
    message = "Username cannot be a number.";
  } else {
    valid = true;
  }
  return [valid, message];
};

export const validateEmail = (email) => {
  let message = "";
  let valid = false;
  if (email === "") {
    valid = false;
    message = "Email cannot be empty.";
  } else {
    let emailPattern = /^([a-zA-Z0-9\W]+)@(\w+)(\.([a-z]{2,5})){1,2}$/g;
    valid = emailPattern.test(email);
    if (!valid) {
      message = "Please provide a valid email.";
    }
  }
  return [valid, message];
};
