
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Container } from 'react-bootstrap';
import React from 'react'

export default function MyNav() {
    return (
        <>
            <Navbar bg="dark" variant="dark" >
                <Container >
                    <Navbar.Brand href="#home" >
                        <img
                            alt=""
                            src="./images/profile.svg"
                            width="30"
                            height="30"
                            className="d-inline-block  align-top"
                        />{' '}
           Person Info React Bootstrap
            </Navbar.Brand>
                </Container>
            </Navbar>
        </>
    )
}
