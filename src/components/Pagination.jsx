import React, { useState } from "react";
import { nanoid } from "nanoid";

export default function Pagination({
  rowsPerPage,
  currentPage,
  totalEntries,
  paginate,
}) {
  const pageNumbers = [];
  const [finalPage] = useState(Math.floor(totalEntries / rowsPerPage));

  for (let i = 0; i < Math.ceil(totalEntries / rowsPerPage); i++) {
    pageNumbers.push(i);
  }

  console.log("total " + totalEntries);

  return (
    <nav>
      <ul className='pagination'>
        <li key={nanoid(3)} className='page-item'>
          <a
            href='!#'
            className='page-link'
            onClick={() => {
              paginate(0);
            }}>
            {"<<"}
          </a>
        </li>
        <li key={nanoid(3)} className='page-item'>
          <a
            href='!#'
            className='page-link'
            onClick={() => {
              if (currentPage > 1) paginate(currentPage - 2);
            }}>
            {"<"}
          </a>
        </li>
        {pageNumbers.map((pageNumber) => (
          <li key={pageNumber} className='page-item'>
            <a
              href='!#'
              className='page-link'
              onClick={() => paginate(pageNumber)}>
              {pageNumber + 1}
            </a>
          </li>
        ))}
        <li key={nanoid(3)} className='page-item'>
          <a
            href='!#'
            className='page-link'
            onClick={() => {
              if (currentPage < finalPage) paginate(currentPage);
            }}>
            {">"}
          </a>
        </li>
        <li key={nanoid(3)} className='page-item'>
          <a
            href='!#'
            className='page-link'
            onClick={() => paginate(finalPage)}>
            {">>"}
          </a>
        </li>
      </ul>
    </nav>
  );
}
