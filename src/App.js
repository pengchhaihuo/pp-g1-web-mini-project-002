import "bootstrap/dist/css/bootstrap.min.css";
import { nanoid } from "nanoid";
import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import moment from "moment";
import MyNav from "./components/MyNav";
import MyForm from "./components/MyForm";
import MyTable from "./components/MyTable.jsx";
import ListCard from "./components/ListCard";
import Toggle from "./components/Toggle";

let defaultUserArray = [
  {
    id: nanoid(6),
    name: "Chester",
    gender: "Male",
    email: "bennington@gmail.com",
    job: ["Developer", "Teacher"],
    createdAt: "2 months ago",
    updatedAt: "Yesterday",
  },
  {
    id: nanoid(6),
    name: "Mike",
    gender: "Male",
    email: "shinoda@gmail.com",
    job: ["Teacher"],
    createdAt: "3 months ago",
    updatedAt: "5 days ago",
  },
  {
    id: nanoid(6),
    name: "Jasmin",
    gender: "Female",
    email: "jasmin1998@gmail.com",
    job: ["Student"],
    createdAt: "5 days ago",
    updatedAt: "Today",
  },
];

function App() {
  const [users, setUsers] = useState(defaultUserArray);
  const [formUser, setFormUser] = useState({});
  const [isUpdating, setUpdating] = useState(false);

  function handleRemoveItem(id) {
    const temp = users.filter((user) => user.id !== id);
    setUsers(temp);
  }

  function handleUpdateItem(user) {
    setUpdating(true);
    setFormUser(user);
    // console.log(user);
  }

  function handleAddItem(e, user) {
    e.preventDefault();
    const randomId = nanoid(6);

    const temp = {
      id: randomId,
      name: user.name,
      gender: user.gender,
      email: user.email,
      job: user.job,
      createdAt: moment().format("YYYY-MM-DD h:mm:ss a"),
      updatedAt: user.updatedAt,
    };
    setUsers((users) => [...users, temp]);
  }

  return (
    <>
      <MyNav />
      <Container>
        <MyForm
          onAdd={handleAddItem}
          formUser={formUser}
          isUpdating={isUpdating}
        />
        <Toggle />
        <div className='py-2'>
          <div id='table'>
            <MyTable
              users={users}
              onDelete={handleRemoveItem}
              onUpdate={handleUpdateItem}
            />
          </div>
          <div id='card' style={{ display: "none" }}>
            <ListCard users={users} onDelete={handleRemoveItem} />
          </div>
        </div>
      </Container>
    </>
  );
}

export default App;
